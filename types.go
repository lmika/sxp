// Main types
//

package main

// An abstract iterator over nodes 
type Iter interface {
    // Returns true if there is another node in this iterator
    Next()      bool

    // Returns the next node and advances the iterator
    Node()      Node
}

// An abstract XML node
type Node interface {
    // Returns the node's string value
    Value()     string
}

// An abstract selector
type Selector interface {
    // Returns true if there are any results that match this selector
    HasAny(context Node)    bool

    // Returns an iterator over all the nodes in the results
    Iter(context Node)      Iter

    // Returns the string value of the first node
    String(context Node)    string
}