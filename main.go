// Simple XPath
//
// An unbelievabily simple XPath selector and processor

package main

import (
    "os"
    "log"

    "github.com/jessevdk/go-flags"
)

// Command line options
type Opts struct {
    Selector    string    `short:"e" description:"an XPath expression"`
    Template    string    `short:"t" description:"run processors through a template"`
}

var opts Opts

func readFileAsXml(fileName string) (Node, error) {
    file, err := os.Open(fileName)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    return ReadFromXML(file)
}

func main() {
    
    args, err := flags.Parse(&opts)
    if err != nil {
        log.Fatal(err)
    }

    propSel, err := NewXPathSelector(opts.Selector)
    if err != nil {
        log.Fatal(err)
    }

    // Sets the selector pipeline
    pipeline := &SelectorProcessor{ Selector: propSel }

    // Sets the processing
    if opts.Template != "" {
        pipeline.SetNext(&TemplateExpanderProcessor{Template: opts.Template})
    } else {
        pipeline.SetNext(&NodeValueProcessor{})
    }

    for _, file := range args {
        dom, err := readFileAsXml(file)
        if err != nil {
            log.Println(err)
            continue
        }

        err = pipeline.Do(dom)
        if err != nil {
            log.Println(err)
        }
    }

    /*
    file, err := os.Open("/home/accounts/lmika/projects/openwis-assoc/owdev-vagrant/artefacts/dataservices/conf/openwis-dataservice-jndi-service.xml")
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    dom, err := ReadFromXML(file)
    if err != nil {
        log.Fatal(err)
    }

    propSel, err := NewXPathSelector("//property")
    if err != nil {
        log.Fatal(err)
    }
    keySel, err := NewXPathSelector("key")
    if err != nil {
        log.Fatal(err)
    }
    valueSel, err := NewXPathSelector("value")
    if err != nil {
        log.Fatal(err)
    }
    _ = valueSel

    for propItr := propSel.Iter(dom); propItr.Next(); {
        node := propItr.Node()

        fmt.Println(keySel.String(node))
    }
    */
}