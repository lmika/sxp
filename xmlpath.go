package main

import (
    "io"
    "launchpad.net/xmlpath"
)

// Implementation of the types using "launchpad.net/xmlpath"

type xmlPathNode struct {
    node *xmlpath.Node
}

// Reads an XML document from the reader and returns the root node
func ReadFromXML(r io.Reader) (Node, error) {
    doc, err := xmlpath.Parse(r)
    if err != nil {
        return nil, err
    } else {
        return &xmlPathNode{doc}, nil
    }
}

// Returns the node's string value
func (xp *xmlPathNode) Value() string {
    return xp.node.String()
}

type xmlPathIter struct {
    iter *xmlpath.Iter
}

// Returns true if there is another node in this iterator
func (xi *xmlPathIter) Next() bool {
    return xi.iter.Next()
}

// Returns the next node and advances the iterator
func (xi *xmlPathIter) Node() Node {
    return &xmlPathNode{xi.iter.Node()}
}


type xmlSelector struct {
    path *xmlpath.Path
}

// Builds a new selector from an XPath expression
func NewXPathSelector(expr string) (Selector, error) {
    sel, err := xmlpath.Compile(expr)
    if err != nil {
        return nil, err
    } else {
        return &xmlSelector{sel}, nil
    }
}

// Returns true if there are any results that match this selector
func (xs *xmlSelector) HasAny(context Node) bool {
    return xs.path.Exists(context.(*xmlPathNode).node)
}

// Returns an iterator over all the nodes in the results
func (xs *xmlSelector) Iter(context Node) Iter {
    return &xmlPathIter{xs.path.Iter(context.(*xmlPathNode).node)}
}

// Returns the string value of the first node
func (xs *xmlSelector) String(context Node) string {
    str, _ := xs.path.String(context.(*xmlPathNode).node)
    return str
}