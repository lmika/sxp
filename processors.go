package main

import (
    "fmt"
    "os"
)

// A processor for nodes.  These are arranged as a pipeline
type Processor interface {
    // Handle a node.  Returns nil or an error
    Do(n Node) error
}

// A processor that can form a pipeline
type PipelineProcessor interface {
    Processor
    SetNext(n Processor)
}


// Base type for pipeline processors
type BasePipelineProcessor struct {
    Next    Processor
}

func (bpp *BasePipelineProcessor) SetNext(n Processor) {
    bpp.Next = n
}


// A pipeline processor that takes a selector and will call the chained processors
// for each node matching the selector in the pipeline
type SelectorProcessor struct {
    BasePipelineProcessor
    Selector    Selector
}

func (sp *SelectorProcessor) Do(n Node) error {
    for itr := sp.Selector.Iter(n); itr.Next(); {
        node := itr.Node()
        if sp.Next != nil {
            err := sp.Next.Do(node)
            if err != nil {
                return err
            }
        }
    }
    return nil
}

// A processor that will print the value of nodes to stdout.
type NodeValueProcessor struct {
}

func (nvp *NodeValueProcessor) Do(n Node) error {
    fmt.Println(n.Value())
    return nil
}


// A processor that will expand values wrapped around ${} with node values
// and will print the result to stdout
type TemplateExpanderProcessor struct {
    Template            string
    compiledSelectors   map[string]Selector
}

func (tep *TemplateExpanderProcessor) Do(n Node) error {
    // Initialize the cache if not done already
    if tep.compiledSelectors == nil {
        tep.compiledSelectors = make(map[string]Selector)
    }

    // Expand the selectors
    fmt.Println(os.Expand(tep.Template, func(s string) string {
        return tep.evaluateExpansion(s, n)
    }))
    return nil
}

func (tep *TemplateExpanderProcessor) evaluateExpansion(expr string, n Node) string {
    selector, hasSelector := tep.compiledSelectors[expr]
    if !hasSelector {
        selector, _ := NewXPathSelector(expr)
        tep.compiledSelectors[expr] = selector
    }

    if selector != nil {
        return selector.String(n)
    } else {
        return ""
    }
}